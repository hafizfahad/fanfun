import 'package:get/get.dart';

class AppController extends GetxController {
  bool loaderProgressCheck = false;
  void updateLoaderProgressCheck({bool updatedValue}) {
    loaderProgressCheck = updatedValue;
    update();
  }
}
