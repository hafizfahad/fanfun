import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/screens/bottom_nav_bar_page.dart';
import 'package:fanfun/screens/login_screen.dart';
import 'package:flutter/material.dart';

class ScreenController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (box.hasData('isLogin')) {
      return BottomNavBAr();
    } else {
      return LoginScreen();
    }
  }
}
