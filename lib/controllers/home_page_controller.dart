import 'package:get/get.dart';

class HomeController extends GetxController {
  bool getCategoryCheck = false;
  void changeGetCategoryCheck({bool updatedValue}) {
    getCategoryCheck = updatedValue;
    update();
  }
}
