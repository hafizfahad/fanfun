import 'package:fanfun/controllers/app_controller.dart';
import 'package:fanfun/controllers/home_page_controller.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:get_storage/get_storage.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_fonts/google_fonts.dart';

import 'screens/splash_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  await Firebase.initializeApp();

  final FirebaseAnalytics analytics = FirebaseAnalytics();

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  //-----load-configurations-from-local-json
  await GlobalConfiguration().loadFromAsset('configurations');
  smartLookSetupKey = GlobalConfiguration().get('smartLookSetupKey').toString();
  SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]);
  Get.put(AppController());
  Get.put(HomeController());
  runApp(GetMaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorObservers: <FirebaseAnalyticsObserver>[
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      theme: ThemeData(
          scaffoldBackgroundColor: const Color(0xff202328),
          primaryColor: const Color(0xff202328),
          unselectedWidgetColor: Colors.white,
          textTheme: TextTheme(
            headline1: GoogleFonts.inter(
              color: Colors.white,
              fontSize: 18,
            ),
            headline2: GoogleFonts.inter(
              color: Colors.white,
              fontSize: 40.0,
              fontWeight: FontWeight.w200,
            ),
            headline3: GoogleFonts.inter(
              color: Colors.white,
              fontSize: 16,
            ),
            headline4: GoogleFonts.inter(
              fontWeight: FontWeight.w400,
              color: const Color(0xff5E6671),
              fontSize: 18,
            ),
            headline5: GoogleFonts.inter(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 24,
            ),
          )),
      home: SplashScreen()));
}
