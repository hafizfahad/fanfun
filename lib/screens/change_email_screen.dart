import 'package:fanfun/data/global_decoration_data.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ChangeEmailScreen extends StatefulWidget {
  const ChangeEmailScreen({Key key}) : super(key: key);

  @override
  _ChangeEmailScreenState createState() => _ChangeEmailScreenState();
}

class _ChangeEmailScreenState extends State<ChangeEmailScreen> {
  final TextEditingController changeEmailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Change Email',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 20, 15, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Current Email:',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(color: customLightGreyColor),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'abc@gmail.com',
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(0, 25, 0, 4),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'New Email:',
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          .copyWith(color: customLightGreyColor),
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                child: Material(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: TextFormField(
                    style: const TextStyle(color: Colors.white),
                    controller: changeEmailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      isDense: true,
                      filled: true,
                      fillColor: customLightGreyColor,
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 15.0, horizontal: 10.0),
                      focusedBorder: customTextFieldFocusedBorder,
                      border: customTextFieldBorder,
                    ),
                    validator: (String value) {
                      const String pattern =
                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                      final RegExp regex = RegExp(pattern);
                      if (value.isEmpty) {
                        return 'Field Required';
                      } else if (!regex.hasMatch(value)) {
                        return 'Please make sure'
                            ' your email address is valid';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: const LinearGradient(
                      colors: <Color>[
                        customGradientFirstColor,
                        customGradientSecondColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'Change',
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          .copyWith(fontSize: 18),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
