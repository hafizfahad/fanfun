import 'dart:developer';

import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CelebrityReviewScreen extends StatefulWidget {
  const CelebrityReviewScreen({Key key}) : super(key: key);

  @override
  _CelebrityReviewScreenState createState() => _CelebrityReviewScreenState();
}

class _CelebrityReviewScreenState extends State<CelebrityReviewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Reviews',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: ListView(
          children: List<Widget>.generate(4, (int index) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(15, 16, 15, 0),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: customPrimaryGreyColor,
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RatingBar(
                        initialRating: 3.5,
                        allowHalfRating: true,
                        itemSize: 15,
                        ratingWidget: RatingWidget(
                          full: const Icon(
                            Icons.star,
                            color: customCategoryYellowColor,
                          ),
                          half: const Icon(Icons.star_half,
                              color: customCategoryYellowColor),
                          empty: const Icon(Icons.star_border_purple500_sharp,
                              color: customCategoryYellowColor),
                        ),
                        onRatingUpdate: (double rating) {
                          log('$rating');
                        },
                      ),
                      Text(
                        'Lorem ipsum dolor sit amet, '
                        'consectetur adipiscing elit,'
                        ' sed do eiusmod tempor incididunt'
                        ' ut labore et dolore magna aliqua.',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(fontSize: 14),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
