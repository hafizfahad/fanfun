import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/data/global_decoration_data.dart';
import 'package:fanfun/screens/celebrity_profile_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<dynamic> queryResultSet = <dynamic>[];
  List<dynamic> tempSearchStore = <dynamic>[];

  List<dynamic> productListForDisplay1 = <dynamic>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: SvgPicture.asset(
                'assets/app_icons/arrows_icon/arrow_left.svg',
                color: Colors.white,
                fit: BoxFit.fill,
              ),
            ),
          ),
          elevation: 0,
          title: Text(
            'Search',
            style: Theme.of(context).textTheme.headline1,
          ),
          centerTitle: true,
        ),
        body: Wrap(
          children: List<Widget>.generate(productListForDisplay1.length + 1,
              (int index) {
            return index == 0
                ? _searchBar()
                : SizedBox(
                    width: MediaQuery.of(context).size.width * .5,
                    child: _listItem(index - 1));
          }),
        ));
  }

  Widget _searchBar() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 20, 15, 16),
      child: Material(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).scaffoldBackgroundColor,
        child: TextFormField(
          style: Theme.of(context).textTheme.headline3,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
              hintText: 'Search',
              hintStyle: Theme.of(context).textTheme.headline3,
              isDense: true,
              filled: true,
              fillColor: customLightGreyColor,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
              focusedBorder: customTextFieldFocusedBorder,
              border: customTextFieldBorder,
              prefixIcon: const Icon(
                Icons.search,
                color: Colors.white,
                size: 25,
              )),
          onChanged: (String val) {
            setState(() {
              productListForDisplay1 = <dynamic>[];
            });
            val = val.toLowerCase();

            getCategoriesList.where((dynamic product) {
              final String productName =
                  product['name'].toString().toLowerCase();
              if (productName.contains(val)) {
                setState(() {
                  productListForDisplay1.add(product);
                });
              }
              return productName.contains(val);
            }).toList();

            if (val.isEmpty) {
              setState(() {
                productListForDisplay1 = <dynamic>[];
              });
            }
          },
          validator: (String value) {
            if (value.isEmpty) {
              return 'Field Required';
            } else {
              return null;
            }
          },
        ),
      ),
    );
  }

  Widget _listItem(int index) {
    return Center(
      child: InkWell(
        onTap: () {
          Get.to(CelebrityProfileScreen());
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: index % 2 == 0
                  ? const EdgeInsets.fromLTRB(0, 15, 0, 0)
                  : const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: Container(
                height: 230,
                width: 150,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 170,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Image.asset(
                          'assets/seventh.jpg',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Profile Name',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: SvgPicture.asset(
                                  'assets/app_icons/other_icon/video_action.svg',
                                  width: 20,
                                  color: Colors.white),
                            ),
                            Text(
                              '250',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontSize: 14),
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: SvgPicture.asset(
                                  'assets/app_icons/other_icon/review_action.svg',
                                  width: 20,
                                  color: Colors.white),
                            ),
                            Text(
                              '250',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontSize: 14),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: index % 2 == 0
                  ? const EdgeInsets.fromLTRB(0, 15, 0, 0)
                  : const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: SizedBox(
                height: 230,
                width: 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 110,
                    ),
                    Container(
                      height: 25,
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: categoryColorsList[1]),
                      child: Center(
                        child: AutoSizeText(
                          '${productListForDisplay1[index]['name']}',
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
