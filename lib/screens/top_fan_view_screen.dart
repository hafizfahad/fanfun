import 'package:fanfun/component/custom_gradient_button.dart';
import 'package:fanfun/component/join_club_bottom_sheet.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class TopFanViewScreen extends StatefulWidget {
  const TopFanViewScreen({Key key}) : super(key: key);

  @override
  _TopFanViewScreenState createState() => _TopFanViewScreenState();
}

class _TopFanViewScreenState extends State<TopFanViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Top 100 Fan Clubs',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: ListView(
          children: List<Widget>.generate(15, (int index) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: ListTile(
                leading: Container(
                  height: 48,
                  width: 48,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      image: const DecorationImage(
                          image: AssetImage('assets/fifth.jpg'),
                          fit: BoxFit.fill)),
                ),
                title: Text(
                  'Profile Name',
                  style: Theme.of(context).textTheme.headline3,
                ),
                subtitle: Text(
                  '2.5k fans',
                  style: Theme.of(context)
                      .textTheme
                      .headline3
                      .copyWith(fontSize: 14, color: customLightGreyColor),
                ),
                trailing: InkWell(
                  onTap: () {
                    customBottomSheet(context);
                  },
                  child: const SizedBox(
                    width: 100,
                    child: CustomGradientButton(
                      buttonHeight: 40,
                      text: 'Join',
                      textFontSize: 16,
                    ),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
