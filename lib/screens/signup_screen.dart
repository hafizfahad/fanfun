import 'package:fanfun/component/gradient_text.dart';
import 'package:fanfun/controllers/app_controller.dart';
import 'package:fanfun/data/global_decoration_data.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/services/api_urls.dart';
import 'package:fanfun/services/post_services.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({this.showSkip});
  final bool showSkip;
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> signUpFormKey = GlobalKey();
  final TextEditingController signUpUsernameController =
      TextEditingController();
  final TextEditingController signUpPasswordController =
      TextEditingController();

  bool obscureTextValue = true;

  @override
  void initState() {
    super.initState();
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
    signUpEmailController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppController>(
      init: AppController(),
      builder: (AppController appController) => ModalProgressHUD(
        progressIndicator: const CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
        ),
        inAsyncCall: appController.loaderProgressCheck,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              leading: (widget.showSkip != null && widget.showSkip == true)
                  ? InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: SvgPicture.asset(
                          'assets/app_icons/arrows_icon/arrow_left.svg',
                          color: Colors.white,
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  : SizedBox(),
              elevation: 0,
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: SingleChildScrollView(
                      child: SafeArea(
                          child: Form(
                        key: signUpFormKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Image.asset(
                              'assets/splashIcon.png',
                              width: MediaQuery.of(context).size.width * .35,
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .03,
                            ),
                            Text(
                              'Sign Up',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontSize: 24),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .05,
                            ),

                            /// username field
                            Padding(
                                padding: const EdgeInsets.fromLTRB(24, 0, 0, 4),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Nickname',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                )),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 0, 24, 16),
                              child: Material(
                                borderRadius: BorderRadius.circular(10.0),
                                color:
                                    Theme.of(context).scaffoldBackgroundColor,
                                child: TextFormField(
                                  style: const TextStyle(color: Colors.white),
                                  controller: signUpUsernameController,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    filled: true,
                                    fillColor: customLightGreyColor,
                                    contentPadding: const EdgeInsets.symmetric(
                                        vertical: 15.0, horizontal: 10.0),
                                    focusedBorder: customTextFieldFocusedBorder,
                                    border: customTextFieldBorder,
                                  ),
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Field required';
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                            ),

                            /// email field
                            Padding(
                                padding: const EdgeInsets.fromLTRB(24, 0, 0, 4),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Email',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                )),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 0, 24, 16),
                              child: Material(
                                borderRadius: BorderRadius.circular(10.0),
                                color:
                                    Theme.of(context).scaffoldBackgroundColor,
                                child: TextFormField(
                                  style: const TextStyle(color: Colors.white),
                                  controller: signUpEmailController,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    filled: true,
                                    fillColor: customLightGreyColor,
                                    contentPadding: const EdgeInsets.symmetric(
                                        vertical: 15.0, horizontal: 10.0),
                                    focusedBorder: customTextFieldFocusedBorder,
                                    border: customTextFieldBorder,
                                  ),
                                  validator: (String value) {
                                    const String pattern =
                                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                    final RegExp regex = RegExp(pattern);
                                    if (value.isEmpty) {
                                      return 'Field Required';
                                    } else if (!regex.hasMatch(value)) {
                                      return 'Please make sure your '
                                          'email address is valid';
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                            ),

                            /// password field
                            Padding(
                                padding: const EdgeInsets.fromLTRB(24, 0, 0, 4),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Password',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(color: customLightGreyColor),
                                  ),
                                )),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 0, 24, 24),
                              child: Material(
                                borderRadius: BorderRadius.circular(10.0),
                                color:
                                    Theme.of(context).scaffoldBackgroundColor,
                                child: TextFormField(
                                  style: const TextStyle(color: Colors.white),
                                  controller: signUpPasswordController,
                                  obscureText: obscureTextValue,
                                  decoration: InputDecoration(
                                      isDense: true,
                                      filled: true,
                                      fillColor: customLightGreyColor,
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 15.0, horizontal: 10.0),
                                      focusedBorder:
                                          customTextFieldFocusedBorder,
                                      border: customTextFieldBorder,
                                      suffixIcon: InkWell(
                                          onTap: () {
                                            setState(() {
                                              obscureTextValue =
                                                  !obscureTextValue;
                                            });
                                          },
                                          child: Icon(
                                            obscureTextValue
                                                ? Icons.remove_red_eye
                                                : Icons.visibility_off,
                                            color: Colors.black,
                                          ))),
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Field Required';
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ),

                            /// signup button
                            Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(24, 0, 24, 0),
                                child: InkWell(
                                  onTap: () {
                                    final FocusScopeNode currentFocus =
                                        FocusScope.of(context);
                                    if (!currentFocus.hasPrimaryFocus) {
                                      currentFocus.unfocus();
                                    }
                                    if (signUpFormKey.currentState.validate()) {
                                      Get.find<AppController>()
                                          .updateLoaderProgressCheck(
                                              updatedValue: true);
                                      postMethod(
                                          registrationApi,
                                          <String, dynamic>{
                                            'name':
                                                signUpUsernameController.text,
                                            'email': signUpEmailController.text,
                                            'password':
                                                signUpPasswordController.text,
                                            'password_confirmation':
                                                signUpPasswordController.text,
                                            'terms': true
                                          },
                                          'register',
                                          context,
                                          null);
                                    }
                                  },
                                  child: Container(
                                    height: 50,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      gradient: const LinearGradient(
                                        colors: <Color>[
                                          customGradientFirstColor,
                                          customGradientSecondColor,
                                        ],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Sign Up',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(fontSize: 18),
                                      ),
                                    ),
                                  ),
                                )),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .03,
                            ),

                            /// divider
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                              child: Row(children: <Widget>[
                                const Expanded(
                                  child: Divider(
                                    color: customPrimaryGreyColor,
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(9, 0, 9, 0),
                                  child: Text(
                                    'or',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(
                                            color: customPrimaryGreyColor),
                                  ),
                                ),
                                const Expanded(
                                  child: Divider(
                                    color: customPrimaryGreyColor,
                                  ),
                                ),
                              ]),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .01,
                            ),

                            /// social buttons
                            Padding(
                              padding: const EdgeInsets.fromLTRB(24, 0, 8, 0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Container(
                                    height: 50,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      gradient: const LinearGradient(
                                        colors: <Color>[
                                          customGradientFirstColor,
                                          customGradientSecondColor,
                                        ],
                                        end: Alignment.bottomRight,
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(1.5),
                                      child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: Theme.of(context)
                                                  .scaffoldBackgroundColor),
                                          child: const Center(
                                            child: GradientText(
                                              'Facebook',
                                              fontSize: 18,
                                              gradient: LinearGradient(
                                                colors: <Color>[
                                                  customGradientFirstColor,
                                                  customGradientSecondColor,
                                                ],
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                              ),
                                            ),
                                          )),
                                    ),
                                  )),
                                  const SizedBox(width: 20),
                                  Expanded(
                                      child: Container(
                                    height: 50,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      gradient: const LinearGradient(
                                        colors: <Color>[
                                          customGradientFirstColor,
                                          customGradientSecondColor,
                                        ],
                                        end: Alignment.bottomRight,
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(1.5),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: Theme.of(context)
                                                .scaffoldBackgroundColor),
                                        child: const Center(
                                          child: GradientText(
                                            'Twitter',
                                            fontSize: 18,
                                            gradient: LinearGradient(
                                              colors: <Color>[
                                                customGradientFirstColor,
                                                customGradientSecondColor,
                                              ],
                                              begin: Alignment.topLeft,
                                              end: Alignment.bottomRight,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .05,
                            ),

                            /// signUp option
                            Text(
                              'Already have an account?',
                              style: Theme.of(context).textTheme.headline3,
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .01,
                            ),
                            InkWell(
                              onTap: () {
                                Get.back();
                              },
                              child: const GradientText(
                                'Login',
                                fontSize: 16,
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    customGradientFirstColor,
                                    customGradientSecondColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .01,
                            ),
                          ],
                        ),
                      )),
                    )),
              ),
            )),
      ),
    );
  }
}
