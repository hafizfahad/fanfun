import 'package:fanfun/component/gradient_text.dart';
import 'package:fanfun/screens/thankyou_screen.dart';
import 'package:flutter/material.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import '../data/global_decoration_data.dart';

class EditPaymentCardScreen extends StatefulWidget {
  const EditPaymentCardScreen(
      {this.cardHolder, this.cardNumber, this.cvv, this.expiryDate});
  final String cardNumber, cardHolder, cvv, expiryDate;
  @override
  _EditPaymentCardScreenState createState() => _EditPaymentCardScreenState();
}

class _EditPaymentCardScreenState extends State<EditPaymentCardScreen> {
  final GlobalKey<FormState> paymentFormKey = GlobalKey();

  final TextEditingController cardHolderNameController =
      TextEditingController();
  final TextEditingController cardNumberController = TextEditingController();
  final TextEditingController expiryDateController = TextEditingController();
  final TextEditingController cvvController = TextEditingController();

  bool showBack = false;

  MaskTextInputFormatter expiryMaskFormatter =
      MaskTextInputFormatter(mask: '##/##');
  MaskTextInputFormatter cvvMaskFormatter =
      MaskTextInputFormatter(mask: '#####');
  MaskTextInputFormatter cardNumberMaskFormatter =
      MaskTextInputFormatter(mask: '##### ##### ##### #####');

  FocusNode _focusNode;
  @override
  void initState() {
    super.initState();
    cardHolderNameController.text = widget.cardHolder;
    cardNumberController.text = widget.cardNumber;
    cvvController.text = widget.cvv;
    expiryDateController.text = widget.expiryDate;
    _focusNode = FocusNode();
    _focusNode.addListener(() {
      setState(() {
        _focusNode.hasFocus ? showBack = true : showBack = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          'Edit card',
          style: Theme.of(context)
              .textTheme
              .headline1
              .copyWith(fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: paymentFormKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Container(
                  height: 200,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                      colors: <Color>[
                        customGradientFirstColor.withOpacity(0.4),
                        customGradientSecondColor.withOpacity(0.4),
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text(
                              'CARD HOLDER',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(
                                      color: customWhiteColor, fontSize: 12),
                            ),
                            Text(
                              'Anna Black',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          '9429  3810  4829  5829',
                          style: Theme.of(context).textTheme.headline3.copyWith(
                              fontSize: 24, fontWeight: FontWeight.w600),
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'CARD EXPIRY',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(
                                      color: customWhiteColor, fontSize: 12),
                            ),
                            Text(
                              'CVV',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(
                                      color: customWhiteColor, fontSize: 12),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              '03/21',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontWeight: FontWeight.w700),
                            ),
                            Text(
                              '12345',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(fontWeight: FontWeight.w700),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(height: 15),
                      Text(
                        'Information',
                        style: Theme.of(context).textTheme.headline4,
                      ),

                      ///card holder name
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5, top: 15),
                        child: Text(
                          'Card holder:',
                          style: Theme.of(context).textTheme.headline4.copyWith(
                                fontSize: 16,
                              ),
                        ),
                      ),
                      //body
                      Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        child: TextFormField(
                          controller: cardHolderNameController,
                          style: Theme.of(context).textTheme.headline3,
                          decoration: InputDecoration(
                            isDense: true,
                            filled: true,
                            fillColor: customLightGreyColor,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            focusedBorder: customTextFieldFocusedBorder,
                            border: customTextFieldBorder,
                          ),
                          onChanged: (String value) {
                            setState(() {
                              cardHolderNameController.text = value;
                            });
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Field Required';
                            }
                            return null;
                          },
                        ),
                      ),

                      ///card number
                      //title
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5, top: 15),
                        child: Text(
                          'Card number:',
                          style: Theme.of(context).textTheme.headline4.copyWith(
                                fontSize: 16,
                              ),
                        ),
                      ),
                      //body
                      Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Theme.of(context).scaffoldBackgroundColor,
                        child: TextFormField(
                          controller: cardNumberController,
                          inputFormatters: <TextInputFormatter>[
                            cardNumberMaskFormatter
                          ],
                          style: Theme.of(context).textTheme.headline3,
                          decoration: InputDecoration(
                            isDense: true,
                            filled: true,
                            fillColor: customLightGreyColor,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            focusedBorder: customTextFieldFocusedBorder,
                            border: customTextFieldBorder,
                          ),
                          onChanged: (String value) {
                            setState(() {
                              cardNumberController.text = value;
                            });
                          },
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Field Required';
                            }
                            return null;
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),

                      /// expiry and cvv section
                      Row(
                        children: <Widget>[
                          ///expiry date
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                //title
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 5,
                                  ),
                                  child: Text(
                                    'Expiry date:',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(
                                          fontSize: 16,
                                        ),
                                  ),
                                ),
                                //body
                                Material(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color:
                                      Theme.of(context).scaffoldBackgroundColor,
                                  child: TextFormField(
                                    controller: expiryDateController,
                                    keyboardType: TextInputType.number,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                    inputFormatters: <TextInputFormatter>[
                                      expiryMaskFormatter
                                    ],
                                    decoration: InputDecoration(
                                      isDense: true,
                                      filled: true,
                                      fillColor: customLightGreyColor,
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 15.0, horizontal: 10.0),
                                      focusedBorder:
                                          customTextFieldFocusedBorder,
                                      border: customTextFieldBorder,
                                    ),
                                    onChanged: (String value) {
                                      setState(() {
                                        expiryDateController.text = value;
                                      });
                                    },
                                    validator: (String value) {
                                      if (value.isEmpty) {
                                        return 'Field Required';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),

                          ///cvv
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                //title
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 5,
                                  ),
                                  child: Text(
                                    'CVV:',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(
                                          fontSize: 16,
                                        ),
                                  ),
                                ),
                                //body
                                Material(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color:
                                      Theme.of(context).scaffoldBackgroundColor,
                                  child: TextFormField(
                                    controller: cvvController,
                                    focusNode: _focusNode,
                                    keyboardType: TextInputType.number,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                    inputFormatters: <TextInputFormatter>[
                                      cvvMaskFormatter
                                    ],
                                    decoration: InputDecoration(
                                      isDense: true,
                                      filled: true,
                                      fillColor: customLightGreyColor,
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 15.0, horizontal: 10.0),
                                      focusedBorder:
                                          customTextFieldFocusedBorder,
                                      border: customTextFieldBorder,
                                    ),
                                    onChanged: (String value) {
                                      setState(() {
                                        cvvController.text = value;
                                      });
                                    },
                                    validator: (String value) {
                                      if (value.isEmpty) {
                                        return 'Field Required';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      InkWell(
                        onTap: () {
                          if (paymentFormKey.currentState.validate()) {
                            Get.to(ThankYouScreen());
                          }
                        },
                        child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: const LinearGradient(
                              colors: <Color>[
                                customGradientFirstColor,
                                customGradientSecondColor,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              'Add',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const Center(
                        child: GradientText(
                          'Delete',
                          fontSize: 18,
                          gradient: LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
