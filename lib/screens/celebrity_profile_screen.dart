import 'dart:developer';
import 'package:fanfun/component/custom_gradient_button.dart';
import 'package:fanfun/component/join_club_bottom_sheet.dart';
import 'package:fanfun/screens/celebrity_reviews_screen.dart';
import 'package:fanfun/screens/celebrity_video_screen.dart';
import 'package:fanfun/screens/request_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CelebrityProfileScreen extends StatefulWidget {
  @override
  _CelebrityProfileScreenState createState() => _CelebrityProfileScreenState();
}

class _CelebrityProfileScreenState extends State<CelebrityProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                leading: InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: SvgPicture.asset(
                      'assets/app_icons/arrows_icon/arrow_left.svg',
                      color: Colors.white,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                elevation: 0,
                expandedHeight: MediaQuery.of(context).size.height * .6,
                bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(30),
                  child: Container(
                    height: 30,
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.vertical(
                            top: Radius.circular(30)),
                        color: Theme.of(context).scaffoldBackgroundColor),
                  ),
                ),
                flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    background: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                        image: AssetImage('assets/seventh.jpg'),
                        fit: BoxFit.fill,
                      )),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              const SizedBox(
                                width: 10,
                              ),

                              ///---response-time
                              Expanded(
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '2 days',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(
                                                fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        'Response Time',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),

                              ///---request
                              Expanded(
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '\$25',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(
                                                fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        'Request',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),

                              ///---available-time
                              Expanded(
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '24/7',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(
                                                fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        'Available',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline3
                                            .copyWith(fontSize: 12),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 50,
                          ),
                        ],
                      ),
                    )),
              ),
            ];
          },
          body: Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: ListView(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: InkWell(
                          onTap: () {
                            Get.to(RequestScreen());
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 7, 0),
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                gradient: const LinearGradient(
                                  colors: <Color>[
                                    customGradientFirstColor,
                                    customGradientSecondColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  'Request',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline3
                                      .copyWith(fontSize: 18),
                                ),
                              ),
                            ),
                          ),
                        )),
                        Expanded(
                            child: InkWell(
                          onTap: () {
                            customBottomSheet(context);
                          },
                          child: const Padding(
                            padding: EdgeInsets.fromLTRB(7, 0, 0, 0),
                            child: CustomGradientButton(
                              buttonHeight: 50,
                              textFontSize: 18,
                              text: 'Join Fan Club',
                            ),
                          ),
                        )),
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .05,
                    ),
                    Text(
                      'Actors',
                      style: Theme.of(context).textTheme.headline3.copyWith(
                          fontSize: 14, color: customCategoryYellowColor),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      'Profile Name',
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          .copyWith(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 15,
                    ),

                    ///---rating-bar
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: RatingBar(
                            initialRating: 3.5,
                            allowHalfRating: true,
                            itemSize: 15,
                            ratingWidget: RatingWidget(
                              full: const Icon(
                                Icons.star,
                                color: customCategoryYellowColor,
                              ),
                              half: const Icon(Icons.star_half,
                                  color: customCategoryYellowColor),
                              empty: const Icon(
                                  Icons.star_border_purple500_sharp,
                                  color: customCategoryYellowColor),
                            ),
                            onRatingUpdate: (double rating) {
                              log('$rating');
                            },
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Get.to(const CelebrityReviewScreen());
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 3),
                            child: Text(
                              '50 Reviews',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  .copyWith(
                                      fontSize: 14,
                                      color: customLightGreyColor,
                                      decoration: TextDecoration.underline),
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Bio',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      'Amet minim mollit non deserunt ullamco est sit '
                      'aliqua dolor do amet sint. Velit officia consequat '
                      'duis enim velit mollit. Exercitation'
                      ' veniam consequat sunt nostrud amet.',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Tags',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 7,
                    ),
                    Wrap(
                      children: List<Widget>.generate(6, (int index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 15, top: 8),
                          child: Container(
                            height: 40,
                            decoration: BoxDecoration(
                                color: customLightGreyColor,
                                borderRadius: BorderRadius.circular(10)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Actor',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Videos',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                    Wrap(
                      children: List<Widget>.generate(3, (int index) {
                        return Padding(
                          padding: index % 2 == 0
                              ? const EdgeInsets.fromLTRB(0, 15, 3, 0)
                              : const EdgeInsets.fromLTRB(3, 15, 0, 0),
                          child: InkWell(
                            onTap: () {
                              Get.to(const CelebrityVideoScreen());
                            },
                            child: Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width * .45,
                              decoration: const BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage('assets/video_demo.png'))),
                            ),
                          ),
                        );
                      }),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    InkWell(
                      onTap: () {
                        Get.to(RequestScreen());
                      },
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Request',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    InkWell(
                      onTap: () {
                        customBottomSheet(context);
                      },
                      child: const CustomGradientButton(
                        buttonHeight: 50,
                        textFontSize: 18,
                        text: 'Join Fan Club',
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
