import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/screens/notification_screen.dart';
import 'package:fanfun/screens/request_screen.dart';
import 'package:fanfun/screens/search_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class OrderHistoryScreen extends StatefulWidget {
  @override
  _OrderHistoryScreenState createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 4, vsync: this);
  }

  List<Map<String, dynamic>> completedSectionList = <Map<String, dynamic>>[
    <String, dynamic>{'name': 'Cameron Williamson', 'rate': 200},
    <String, dynamic>{'name': 'Joao Doe', 'rate': 300},
    <String, dynamic>{'name': 'Robbit Roe', 'rate': 100},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        title: AutoSizeText(
          'My Orders',
          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 24),
        ),
        actions: <Widget>[
          InkWell(
            onTap: () {
              Get.to(const SearchScreen());
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 30, 0),
              child: SvgPicture.asset(
                'assets/app_icons/navigation_bar_icon/search.svg',
                color: Colors.white,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Get.to(NotificationScreen());
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
              child: SvgPicture.asset(
                'assets/app_icons/navigation_bar_icon/notifications.svg',
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              colors: <Color>[Color(0xFF1E2026), Color(0xFF23252E)],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.9,
                child: FittedBox(
                  child: SizedBox(
                    height: 40,
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: TabBar(
                      isScrollable: true,
                      controller: tabController,
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.white,
                      labelStyle:
                          Theme.of(context).textTheme.headline3.copyWith(
                                fontWeight: FontWeight.w400,
                              ),
                      indicator: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            customGradientFirstColor,
                            customGradientSecondColor,
                          ],
                          end: Alignment.bottomRight,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      tabs: <Widget>[
                        Center(
                          child: Text(
                            'All',
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                        Center(
                          child: AutoSizeText(
                            'Open',
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                        Center(
                          child: AutoSizeText(
                            'Completed',
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                        Center(
                          child: AutoSizeText(
                            'Expired',
                            style:
                                Theme.of(context).textTheme.headline3.copyWith(
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Expanded(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height * .73,
                  child: TabBarView(
                    controller: tabController,
                    children: <Widget>[
                      ///all section
                      Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AutoSizeText(
                              'Your orders are empty!',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontWeight: FontWeight.w700),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            InkWell(
                              onTap: () {
                                Get.to(RequestScreen());
                              },
                              child: Container(
                                height: 50,
                                width: MediaQuery.of(context).size.width * 0.45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: const LinearGradient(
                                    colors: <Color>[
                                      customGradientFirstColor,
                                      customGradientSecondColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'Request Now',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                      ///open section
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                        child: SingleChildScrollView(
                          child: Column(
                              children: completedSectionList
                                  .map((Map<String, dynamic> e) => Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 15.0),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .95,
                                          decoration: BoxDecoration(
                                              color: customPrimaryGreyColor,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: 25,
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              40),
                                                      child: Image.asset(
                                                          'assets/profile.jpg')),
                                                ),
                                                Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Text(
                                                          '12-05-2021',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                          maxLines: 2,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          e['name'].toString(),
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                          maxLines: 2,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          '\$${e['rate']}',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color:
                                                                      customOrangeTextColor),
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          'Lorem ipsum dolor sit amet,'
                                                          ' consectetur adipiscing elit, sed do eiusmod.',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 14),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 30,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color:
                                                          customCategoryBlueColor),
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(8, 0, 8, 0),
                                                    child: Center(
                                                        child: Text(
                                                      'Open',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3
                                                          .copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 12),
                                                    )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ))
                                  .toList()),
                        ),
                      ),

                      ///compelted section
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                        child: SingleChildScrollView(
                          child: Column(
                              children: completedSectionList
                                  .map((Map<String, dynamic> e) => Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 15.0),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .95,
                                          decoration: BoxDecoration(
                                              color: customPrimaryGreyColor,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: 25,
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              40),
                                                      child: Image.asset(
                                                          'assets/profile.jpg')),
                                                ),
                                                Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Text(
                                                          '12-05-2021',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                          maxLines: 2,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          e['name'].toString(),
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                          maxLines: 2,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          '\$${e['rate']}',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color:
                                                                      customOrangeTextColor),
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          'Lorem ipsum dolor sit amet,'
                                                          ' consectetur adipiscing elit, sed do eiusmod.',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 14),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 30,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color:
                                                          customCategoryGreenColor),
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(8, 0, 8, 0),
                                                    child: Center(
                                                        child: Text(
                                                      'Completed',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3
                                                          .copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 12),
                                                    )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ))
                                  .toList()),
                        ),
                      ),

                      ///expired section
                      Padding(
                        padding: const EdgeInsets.fromLTRB(18, 20, 18, 0),
                        child: SingleChildScrollView(
                          child: Column(
                              children: completedSectionList
                                  .map((Map<String, dynamic> e) => Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 15.0),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .95,
                                          decoration: BoxDecoration(
                                              color: customPrimaryGreyColor,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: 25,
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              40),
                                                      child: Image.asset(
                                                          'assets/profile.jpg')),
                                                ),
                                                Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 10),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        Text(
                                                          '12-05-2021',
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline4
                                                                  .copyWith(
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                          maxLines: 2,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          e['name'].toString(),
                                                          style:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .headline3
                                                                  .copyWith(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                          maxLines: 2,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          '\$${e['rate']}',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color:
                                                                      customOrangeTextColor),
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          'Lorem ipsum dolor sit amet,'
                                                          ' consectetur adipiscing elit, sed do eiusmod.',
                                                          style: Theme.of(
                                                                  context)
                                                              .textTheme
                                                              .headline3
                                                              .copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize: 14),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  height: 30,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color:
                                                          customCategoryRedColor),
                                                  child: Padding(
                                                    padding: const EdgeInsets
                                                        .fromLTRB(8, 0, 8, 0),
                                                    child: Center(
                                                        child: Text(
                                                      'Expired',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline3
                                                          .copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              fontSize: 12),
                                                    )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ))
                                  .toList()),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
