import 'dart:async';
import 'dart:developer';
import 'dart:io' show Platform;
import 'package:fanfun/controllers/init_controller.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smartlook/smartlook.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void navigator() {
    Navigator.of(context).pushReplacement(PageRouteBuilder<dynamic>(
        pageBuilder: (_, __, ___) => ScreenController(),
        transitionDuration: const Duration(milliseconds: 2000),
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return Opacity(
            opacity: animation.value,
            child: child,
          );
        }));
  }

  /// Set timer SplashScreenTemplate1
  Future<Timer> _timer() async {
    return Timer(const Duration(milliseconds: 4300), navigator);
  }

  @override
  void initState() {
    super.initState();
    _timer();

    ///-------------------------------------------
    final SetupOptions options = (SetupOptionsBuilder(smartLookSetupKey)
          ..Fps = 2
          ..StartNewSession = true)
        .build();
    Smartlook.setupAndStartRecording(options);
    // calling all functions to make sure nothing crashes
    Smartlook.setEventTrackingMode(EventTrackingMode.FULL_TRACKING);
    final List<EventTrackingMode> eventTrackingModeList =
        List<EventTrackingMode>.filled(2, EventTrackingMode.NO_TRACKING,
            growable: true);
    eventTrackingModeList[0] = EventTrackingMode.FULL_TRACKING;
    eventTrackingModeList[1] = EventTrackingMode.IGNORE_USER_INTERACTION;
    Smartlook.setEventTrackingModes(eventTrackingModeList);
    Smartlook.registerIntegrationListener(CustomIntegrationListener());
    Smartlook.setUserIdentifier(
        'FanFun User', <String, dynamic>{'flutter-usr-prop': 'valueX'});
    Smartlook.setGlobalEventProperty('flutter_global', 'value_', true);
    Smartlook.enableWebviewRecording(true);
    Smartlook.enableCrashlytics(true);
    Smartlook.setReferrer('referer', 'source');
    Smartlook.getDashboardSessionUrl(true);

    ///-------------------------------------------
    if (!box.hasData('fcm_token')) {
      FirebaseMessaging.instance.getToken().then((dynamic value) {
        box.write('fcm_token', value);
        log('FirebaseMessagingToken------------->> $value');
        log('PlatForm------------->> ${Platform.operatingSystem}');
      });
    }

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {}
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      log('FirebaseMessage------------->> $message');
      Get.snackbar(
          '${message.notification.title}', '${message.notification.body}',
          colorText: Colors.white);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log('A new onMessageOpenedApp event was published!');
    });
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      log('FirebaseMessagingBackground------------->> $message');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration:
              BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/splashIcon.png'),
            ],
          )),
    );
  }
}

class CustomIntegrationListener implements IntegrationListener {
  @override
  void onSessionReady(String dashboardSessionUrl) {
    log('DashboardUrl:');
    log(dashboardSessionUrl);
  }

  @override
  void onVisitorReady(String dashboardVisitorUrl) {
    log('DashboardVisitorUrl:');
    log(dashboardVisitorUrl);
  }
}
