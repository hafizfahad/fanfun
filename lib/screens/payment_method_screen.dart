import 'package:fanfun/component/gradient_text.dart';
import 'package:fanfun/screens/thankyou_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaymentMethodScreen extends StatefulWidget {
  const PaymentMethodScreen({Key key}) : super(key: key);

  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen> {
  int value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Payment Method',
          style: Theme.of(context)
              .textTheme
              .headline1
              .copyWith(fontWeight: FontWeight.w400),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Select card:',
              style: Theme.of(context).textTheme.headline4,
            ),
            const SizedBox(
              height: 20,
            ),
            // ignore: always_specify_types
            Row(
              children: <Widget>[
                // ignore: always_specify_types
                Radio(
                    activeColor: customOrangeColor,
                    autofocus: true,
                    value: 1,
                    groupValue: value,
                    onChanged: (int newValue) {
                      setState(() {
                        value = newValue;
                      });
                    }),
                Text(
                  'Personal',
                  style: Theme.of(context).textTheme.headline1,
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: <Color>[
                    customGradientFirstColor.withOpacity(0.4),
                    customGradientSecondColor.withOpacity(0.4),
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          'CARD HOLDER',
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              .copyWith(color: customWhiteColor, fontSize: 12),
                        ),
                        Text(
                          'Anna Black',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      '9429  3810  4829  5829',
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          .copyWith(fontSize: 24, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'CARD EXPIRY',
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              .copyWith(color: customWhiteColor, fontSize: 12),
                        ),
                        Text(
                          'CVV',
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              .copyWith(color: customWhiteColor, fontSize: 12),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          '03/21',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(fontWeight: FontWeight.w700),
                        ),
                        Text(
                          '12345',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(fontWeight: FontWeight.w700),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const Center(
              child: GradientText(
                'Other card',
                fontSize: 18,
                gradient: LinearGradient(
                  colors: <Color>[
                    customGradientFirstColor,
                    customGradientSecondColor,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            InkWell(
              onTap: () {
                Get.to(ThankYouScreen());
              },
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: customLightGreyColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: Text(
                    'Request \$25',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: customPrimaryGreyColor),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
