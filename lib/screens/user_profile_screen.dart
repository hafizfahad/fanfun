import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/screens/eidt_user_profile_screen.dart';
import 'package:fanfun/screens/notification_screen.dart';
import 'package:fanfun/screens/user_settings_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class UserProfileScreen extends StatelessWidget {
  final List<Map<String, dynamic>> fanClubsList = <Map<String, dynamic>>[
    <String, dynamic>{'name': 'Emilia Clarke', 'fans': '9.3k'},
    <String, dynamic>{'name': 'Esther Howard', 'fans': '3.3k'},
    <String, dynamic>{'name': 'Guy Hawkins', 'fans': '2.1k'},
    <String, dynamic>{'name': 'Bessie Cooper', 'fans': '4.1k'}
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        title: AutoSizeText(
          'My Profile',
          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 24),
        ),
        actions: <Widget>[
          InkWell(
            onTap: () {
              Get.to(NotificationScreen());
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 30, 0),
              child: SvgPicture.asset(
                'assets/app_icons/navigation_bar_icon/notifications.svg',
                color: Colors.white,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Get.to(const UserSettingsScreen());
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
              child: SvgPicture.asset(
                'assets/app_icons/navigation_bar_icon/settings.svg',
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 120,
              width: double.infinity,
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 50,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(60),
                        child: Image.asset('assets/profile.jpg')),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Annette Black',
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(fontWeight: FontWeight.w700),
                      ),
                      Text(
                        '@anna_black',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(fontWeight: FontWeight.w400),
                      ),
                      InkWell(
                          onTap: () {
                            Get.to(EditUserProfileScreen());
                          },
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(
                                colors: <Color>[
                                  customGradientFirstColor,
                                  customGradientSecondColor,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(7.0),
                                child: SvgPicture.asset(
                                  'assets/app_icons/action_icon/edit.svg',
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ))
                    ],
                  ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              child: Text(
                'Fan Clubs',
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontWeight: FontWeight.w700),
              ),
            ),
            ...fanClubsList
                .map((Map<String, dynamic> e) => ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 25,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(40),
                            child: Image.asset('assets/profile.jpg')),
                      ),
                      title: Text(
                        e['name'].toString(),
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(fontWeight: FontWeight.w700),
                      ),
                      subtitle: Text(
                        '${e['fans']}fans',
                        style: Theme.of(context)
                            .textTheme
                            .headline4
                            .copyWith(fontSize: 14),
                      ),
                      trailing: Container(
                        width: 70,
                        height: 30,
                        decoration: BoxDecoration(
                          gradient: const LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: Center(
                              child: Text(
                            'Joined',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(
                                    fontWeight: FontWeight.w400, fontSize: 12),
                          )),
                        ),
                      ),
                    ))
                .toList()
          ],
        )),
      ),
    );
  }
}
