import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/data/global_decoration_data.dart';
import 'package:fanfun/screens/request_confirmation_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  List<String> proNounList = <String>[
    'Eles',
    'Ele',
    'Ela',
    'Vou explicar nas Instruções'
  ];
  List<String> occasionList = <String>[
    'Aniversário',
    'Conselho',
    'Casamento',
    'Zueira'
  ];

  GlobalKey<FormState> requestFormKey = GlobalKey();

  bool identityCheck = true;

  @override
  void initState() {
    super.initState();

    inController.clear();
    friendNameController.clear();
    instructionController.clear();
    pronounValue = '';
    occasionValue = '';
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: SvgPicture.asset(
                'assets/app_icons/arrows_icon/arrow_left.svg',
                color: Colors.white,
                fit: BoxFit.fill,
              ),
            ),
          ),
          elevation: 0,
          title: Text(
            'New Request',
            style: Theme.of(context).textTheme.headline1,
          ),
          centerTitle: true,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Get.back();
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 17, 10, 0),
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .headline3
                      .copyWith(color: customOrangeColor),
                ),
              ),
            )
          ],
        ),
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
          onTap: () {
            final FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration:
                BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
            child: SingleChildScrollView(
                child: Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 10),
                child: Form(
                  key: requestFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: InkWell(
                            onTap: () {
                              setState(() {
                                identityCheck = !identityCheck;
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                              child: Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(
                                    colors: identityCheck
                                        ? <Color>[
                                            customGradientFirstColor,
                                            customGradientSecondColor,
                                          ]
                                        : <Color>[
                                            customPrimaryGreyColor,
                                            customPrimaryGreyColor
                                          ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'For me',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          )),
                          Expanded(
                              child: InkWell(
                            onTap: () {
                              setState(() {
                                identityCheck = !identityCheck;
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(
                                    colors: !identityCheck
                                        ? <Color>[
                                            customGradientFirstColor,
                                            customGradientSecondColor,
                                          ]
                                        : <Color>[
                                            customPrimaryGreyColor,
                                            customPrimaryGreyColor
                                          ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Center(
                                  child: Text(
                                    'For friend',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline3
                                        .copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          )),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const SizedBox(
                            height: 30,
                          ),
                          Text(
                            'De:',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Theme.of(context).scaffoldBackgroundColor,
                            child: TextFormField(
                              style: Theme.of(context).textTheme.headline3,
                              controller: inController,
                              decoration: InputDecoration(
                                isDense: true,
                                filled: true,
                                fillColor: customLightGreyColor,
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                                focusedBorder: customTextFieldFocusedBorder,
                                border: customTextFieldBorder,
                              ),
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Field Required';
                                }
                                return null;
                              },
                            ),
                          ),
                          if (!identityCheck)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                const SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  'Para:',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline3
                                      .copyWith(color: customLightGreyColor),
                                ),
                                const SizedBox(
                                  height: 4,
                                ),
                                Material(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color:
                                      Theme.of(context).scaffoldBackgroundColor,
                                  child: TextFormField(
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                    controller: friendNameController,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      filled: true,
                                      fillColor: customLightGreyColor,
                                      contentPadding:
                                          const EdgeInsets.symmetric(
                                              vertical: 15.0, horizontal: 10.0),
                                      focusedBorder:
                                          customTextFieldFocusedBorder,
                                      border: customTextFieldBorder,
                                    ),
                                    validator: (String value) {
                                      if (value.isEmpty) {
                                        return 'Field Required';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ],
                            ),
                          const SizedBox(
                            height: 15,
                          ),
                          Text(
                            'Pronome:',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButtonFormField<String>(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 5.0),
                                  fillColor: customLightGreyColor,
                                  filled: true,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                ),
                                isExpanded: true,
                                focusColor: Colors.white,
                                dropdownColor: customLightGreyColor,
                                icon: SvgPicture.asset(
                                  'assets/app_icons/arrows_icon/arrow_down.svg',
                                  color: Colors.white,
                                ),
                                iconSize: 10,
                                style: Theme.of(context).textTheme.headline3,
                                iconEnabledColor: Colors.black,
                                items: proNounList
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (String value) {
                                  setState(() {
                                    pronounValue = value;
                                  });
                                },
                                validator: (String value) {
                                  if (value == null) {
                                    return 'Field Required';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Text(
                            'Ocasião:',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButtonFormField<String>(
                                decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 5.0),
                                  fillColor: customLightGreyColor,
                                  filled: true,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                ),
                                isExpanded: true,
                                focusColor: Colors.white,
                                dropdownColor: customLightGreyColor,
                                icon: SvgPicture.asset(
                                  'assets/app_icons/arrows_icon/arrow_down.svg',
                                  color: Colors.white,
                                ),
                                iconSize: 10,
                                style: Theme.of(context).textTheme.headline3,
                                iconEnabledColor: Colors.black,
                                items: occasionList
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (String value) {
                                  setState(() {
                                    occasionValue = value;
                                  });
                                },
                                validator: (String value) {
                                  if (value == null) {
                                    return 'Field Required';
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Text(
                            'Instruções:',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Theme.of(context).scaffoldBackgroundColor,
                            child: TextFormField(
                              maxLines: 5,
                              style: Theme.of(context).textTheme.headline3,
                              controller: instructionController,
                              decoration: InputDecoration(
                                isDense: true,
                                filled: true,
                                fillColor: customLightGreyColor,
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                                focusedBorder: customTextFieldFocusedBorder,
                                border: customTextFieldBorder,
                              ),
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Field Required';
                                }
                                return null;
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          InkWell(
                            onTap: () {
                              if (requestFormKey.currentState.validate()) {
                                Get.to(const RequestConfirmationScreen());
                              }
                            },
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                gradient: const LinearGradient(
                                  colors: <Color>[
                                    customGradientFirstColor,
                                    customGradientSecondColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  'Next',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline3
                                      .copyWith(fontSize: 18),
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )),
          ),
        ),
      ),
    );
  }
}
