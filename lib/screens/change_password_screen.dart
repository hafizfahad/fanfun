import 'package:fanfun/data/global_decoration_data.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final TextEditingController oldPasswordController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  bool oldObscureTextValue = true;
  bool newObscureTextValue = true;
  bool confirmObscureTextValue = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Change Password',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 4),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Current Password:',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(color: customLightGreyColor),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Theme.of(context).scaffoldBackgroundColor,
                      child: TextFormField(
                        style: const TextStyle(color: Colors.white),
                        controller: oldPasswordController,
                        obscureText: oldObscureTextValue,
                        decoration: InputDecoration(
                            isDense: true,
                            filled: true,
                            fillColor: customLightGreyColor,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            focusedBorder: customTextFieldFocusedBorder,
                            border: customTextFieldBorder,
                            suffixIcon: InkWell(
                                onTap: () {
                                  setState(() {
                                    oldObscureTextValue = !oldObscureTextValue;
                                  });
                                },
                                child: Icon(
                                  oldObscureTextValue
                                      ? Icons.remove_red_eye
                                      : Icons.visibility_off,
                                  color: Colors.black,
                                ))),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Field Required';
                          }
                          return null;
                        },
                      )),
                ),
                Padding(
                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 4),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'New Password:',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(color: customLightGreyColor),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Theme.of(context).scaffoldBackgroundColor,
                      child: TextFormField(
                        style: const TextStyle(color: Colors.white),
                        controller: newPasswordController,
                        obscureText: newObscureTextValue,
                        decoration: InputDecoration(
                            isDense: true,
                            filled: true,
                            fillColor: customLightGreyColor,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            focusedBorder: customTextFieldFocusedBorder,
                            border: customTextFieldBorder,
                            suffixIcon: InkWell(
                                onTap: () {
                                  setState(() {
                                    newObscureTextValue = !newObscureTextValue;
                                  });
                                },
                                child: Icon(
                                  newObscureTextValue
                                      ? Icons.remove_red_eye
                                      : Icons.visibility_off,
                                  color: Colors.black,
                                ))),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Field Required';
                          }
                          return null;
                        },
                      )),
                ),
                Padding(
                    padding: const EdgeInsets.fromLTRB(15, 25, 15, 4),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Confirm Password:',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(color: customLightGreyColor),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 40),
                  child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Theme.of(context).scaffoldBackgroundColor,
                      child: TextFormField(
                        style: const TextStyle(color: Colors.white),
                        controller: confirmPasswordController,
                        obscureText: confirmObscureTextValue,
                        decoration: InputDecoration(
                            isDense: true,
                            filled: true,
                            fillColor: customLightGreyColor,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            focusedBorder: customTextFieldFocusedBorder,
                            border: customTextFieldBorder,
                            suffixIcon: InkWell(
                                onTap: () {
                                  setState(() {
                                    confirmObscureTextValue =
                                        !confirmObscureTextValue;
                                  });
                                },
                                child: Icon(
                                  confirmObscureTextValue
                                      ? Icons.remove_red_eye
                                      : Icons.visibility_off,
                                  color: Colors.black,
                                ))),
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Field Required';
                          }
                          return null;
                        },
                      )),
                ),
                InkWell(
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: const LinearGradient(
                          colors: <Color>[
                            customGradientFirstColor,
                            customGradientSecondColor,
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'Change',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(fontSize: 18),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
