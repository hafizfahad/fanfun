import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class TermsOfService extends StatefulWidget {
  const TermsOfService({Key key}) : super(key: key);

  @override
  _TermsOfServiceState createState() => _TermsOfServiceState();
}

class _TermsOfServiceState extends State<TermsOfService> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Terms of Service',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              'Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. '
              'Sunt qui esse pariatur duis deserunt mollit dolore cillum '
              'minim tempor enim. Elit aute irure tempor cupidatat incididunt '
              'sint deserunt ut voluptate aute id deseru.Nulla Lorem mollit '
              'cupidatat irure. Laborum magna nulla duis ullamco cillum dolor.'
              ' Voluptate exercitation incididunt aliquip deserunt reprehenderit '
              'elit laborum.Aliqua id fugiat nostrud irure ex duis ea quis id quis'
              ' ad et. Sunt qui esse pariatur duis deserunt mollit dolore cillum'
              ' minim tempor enim. Elit aute irure tempor cupidatat incididunt sint '
              'deserunt ut volu.Nulla Lorem mollit cupidatat irure. Laborum magna nulla'
              ' duis ullamco cillum dolor. Voluptate exercitation incididunt aliquip'
              ' deserunt reprehenderit elit laborum.Aliqua id fugiat nostrud irure ex duis '
              'ea quis id quis ad et. Sunt qui esse pariatur duis deserunt mollit dolore'
              ' cillum minim tempor enim. Elit aute irure tempor cupidatat incididunt.Nulla '
              'Lorem mollit cupidatat irure. Laborum magna nulla duis ullamco cillum dolor. '
              'Voluptate exercitation incididunt aliquip deserunt reprehenderit elit laborum.',
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
        ),
      ),
    );
  }
}
