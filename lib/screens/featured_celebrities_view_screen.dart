import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/screens/celebrity_profile_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class FeaturedCelebrityViewScreen extends StatefulWidget {
  const FeaturedCelebrityViewScreen({Key key}) : super(key: key);

  @override
  _FeaturedCelebrityViewScreenState createState() =>
      _FeaturedCelebrityViewScreenState();
}

class _FeaturedCelebrityViewScreenState
    extends State<FeaturedCelebrityViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Features',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: SingleChildScrollView(
          child: Wrap(
            children: List<Widget>.generate(15, (int index) {
              return SizedBox(
                width: MediaQuery.of(context).size.width * .5,
                child: Center(
                  child: InkWell(
                    onTap: () {
                      Get.to(CelebrityProfileScreen());
                    },
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: index % 2 == 0
                              ? const EdgeInsets.fromLTRB(0, 15, 0, 0)
                              : const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Container(
                            height: 230,
                            width: 150,
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(15)),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 170,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child: Image.asset(
                                      'assets/seventh.jpg',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  'Profile Name',
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                                const SizedBox(
                                  height: 6,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 5),
                                          child: SvgPicture.asset(
                                              'assets/app_icons/other_icon/video_action.svg',
                                              width: 20,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          '250',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .copyWith(fontSize: 14),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 5),
                                          child: SvgPicture.asset(
                                              'assets/app_icons/other_icon/review_action.svg',
                                              width: 20,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          '250',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3
                                              .copyWith(fontSize: 14),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: index % 2 == 0
                              ? const EdgeInsets.fromLTRB(0, 15, 0, 0)
                              : const EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: SizedBox(
                            height: 230,
                            width: 150,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                const SizedBox(
                                  height: 110,
                                ),
                                Container(
                                  height: 25,
                                  width: 80,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    gradient: const LinearGradient(
                                      colors: <Color>[
                                        customGradientFirstColor,
                                        customGradientSecondColor,
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                    ),
                                  ),
                                  child: Center(
                                    child: AutoSizeText(
                                      'Actor',
                                      softWrap: true,
                                      overflow: TextOverflow.ellipsis,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline3
                                          .copyWith(fontSize: 14),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}
