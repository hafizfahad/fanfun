import 'package:fanfun/component/gradient_text.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/screens/change_email_screen.dart';
import 'package:fanfun/screens/login_screen.dart';
import 'package:fanfun/screens/notifications_settings_screen.dart';
import 'package:fanfun/screens/payment_settings_screen.dart';
import 'package:fanfun/screens/terms_of_service.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'change_password_screen.dart';

class UserSettingsScreen extends StatefulWidget {
  const UserSettingsScreen({Key key}) : super(key: key);

  @override
  _UserSettingsScreenState createState() => _UserSettingsScreenState();
}

class _UserSettingsScreenState extends State<UserSettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Settings',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: ListView(
          children: <Widget>[
            ListTile(
              onTap: () {
                Get.to(const NotificationsSettingsScreen());
              },
              title: Text(
                'Notifications',
                style: Theme.of(context).textTheme.headline1,
              ),
              trailing: SvgPicture.asset(
                'assets/app_icons/arrow_icon/arrow_right.svg',
                color: Colors.white,
              ),
            ),
            InkWell(
              onTap: () {
                Get.to(const ChangeEmailScreen());
              },
              child: ListTile(
                title: Text(
                  'Change Email',
                  style: Theme.of(context).textTheme.headline1,
                ),
                trailing: SvgPicture.asset(
                  'assets/app_icons/arrow_icon/arrow_right.svg',
                  color: Colors.white,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Get.to(const ChangePasswordScreen());
              },
              child: ListTile(
                title: Text(
                  'Change Password',
                  style: Theme.of(context).textTheme.headline1,
                ),
                trailing: SvgPicture.asset(
                  'assets/app_icons/arrow_icon/arrow_right.svg',
                  color: Colors.white,
                ),
              ),
            ),
            ListTile(
              onTap: () {
                Get.to(PaymentSettingsScreen());
              },
              title: Text(
                'Payments',
                style: Theme.of(context).textTheme.headline1,
              ),
              trailing: SvgPicture.asset(
                'assets/app_icons/arrow_icon/arrow_right.svg',
                color: Colors.white,
              ),
            ),
            InkWell(
              onTap: () {
                Get.to(const TermsOfService());
              },
              child: ListTile(
                title: Text(
                  'Terms of service',
                  style: Theme.of(context).textTheme.headline1,
                ),
                trailing: SvgPicture.asset(
                  'assets/app_icons/arrow_icon/arrow_right.svg',
                  color: Colors.white,
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Security and privacy',
                style: Theme.of(context).textTheme.headline1,
              ),
              trailing: SvgPicture.asset(
                'assets/app_icons/arrow_icon/arrow_right.svg',
                color: Colors.white,
              ),
            ),
            InkWell(
              onTap: () {
                box.remove('accessToken');
                box.remove('isLogin');
                Get.offAll(LoginScreen());
              },
              child: const ListTile(
                title: GradientText(
                  'Log out',
                  fontSize: 18,
                  gradient: LinearGradient(
                    colors: <Color>[
                      customGradientFirstColor,
                      customGradientSecondColor,
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
