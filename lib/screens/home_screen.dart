import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/component/custom_gradient_button.dart';
import 'package:fanfun/component/join_club_bottom_sheet.dart';
import 'package:fanfun/controllers/home_page_controller.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/screens/category_view_screen.dart';
import 'package:fanfun/screens/celebrity_profile_screen.dart';
import 'package:fanfun/screens/featured_celebrities_view_screen.dart';
import 'package:fanfun/screens/notification_screen.dart';
import 'package:fanfun/screens/search_screen.dart';
import 'package:fanfun/screens/top_fan_view_screen.dart';
import 'package:fanfun/services/api_urls.dart';
import 'package:fanfun/services/get_services.dart';
import 'package:fanfun/services/post_services.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    getMethod(getCategoryApi, context, 'categories', null);
    if (!box.hasData('fcmTokenSaved')) {
      postMethod(
          fcmTokenApi,
          <String, dynamic>{
            'token': box.read('fcm_token'),
            'platform': Platform.operatingSystem.toString(),
          },
          'fcmToken',
          context,
          null);
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _drawerKey,
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
        title: AutoSizeText(
          'Home',
          style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 24),
        ),
        actions: <Widget>[
          InkWell(
            onTap: () {
              Get.to(const SearchScreen());
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 30, 0),
              child: SvgPicture.asset(
                'assets/app_icons/navigation_bar_icon/search.svg',
                color: Colors.white,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Get.to(NotificationScreen());
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
              child: SvgPicture.asset(
                'assets/app_icons/navigation_bar_icon/notifications.svg',
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: GetBuilder<HomeController>(
        init: HomeController(),
        builder: (HomeController homeController) => Container(
          height: size.height,
          width: size.width,
          decoration: BoxDecoration(
            color: Theme.of(context).scaffoldBackgroundColor,
          ),
          child: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              ///---categories-widget
              if (homeController.getCategoryCheck == false)
                SkeletonLoader(
                  highlightColor: Colors.grey,
                  builder: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: SizedBox(
                        height: 40,
                        width: MediaQuery.of(context).size.width,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: List<Widget>.generate(10, (int index) {
                            return Padding(
                                padding: index == 0
                                    ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                    : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                child: Container(
                                  height: 40,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                ));
                          }),
                        )),
                  ),
                )
              else
                getCategoriesList.isEmpty
                    ? const SizedBox()
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: SizedBox(
                          height: 40,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: getCategoriesList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: InkWell(
                                    onTap: () {
                                      Get.to(CategoryViewScreen(
                                        categoryName: (getCategoriesList[index]
                                                ['name'])
                                            .toString(),
                                        categoryColor:
                                            categoryColorsList[index],
                                      ));
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: categoryColorsList[index]),
                                      child: Center(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: AutoSizeText(
                                            '${getCategoriesList[index]['name']}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline3,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ),

              ///---featured-widget
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 40, 15, 15),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(
                          'Features',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontWeight: FontWeight.bold),
                        )),
                        InkWell(
                          onTap: () {
                            Get.to(const FeaturedCelebrityViewScreen());
                          },
                          child: AutoSizeText(
                            'See all',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 210,
                    width: size.width,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              Get.to(CelebrityProfileScreen());
                            },
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Container(
                                    height: 200,
                                    width: 120,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          height: 140,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            child: Image.asset(
                                              'assets/seventh.jpg',
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          'Profile Name',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        ),
                                        const SizedBox(
                                          height: 6,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/video_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/review_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: SizedBox(
                                    height: 200,
                                    width: 120,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        const SizedBox(
                                          height: 80,
                                        ),
                                        if (!homeController.getCategoryCheck)
                                          SkeletonLoader(
                                            highlightColor: Colors.grey,
                                            builder: Column(
                                              children: <Widget>[
                                                Container(
                                                  height: 25,
                                                  width: 80,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color: Colors.white),
                                                ),
                                              ],
                                            ),
                                          )
                                        else
                                          Container(
                                            height: 25,
                                            width: 80,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              gradient: const LinearGradient(
                                                colors: <Color>[
                                                  customGradientFirstColor,
                                                  customGradientSecondColor,
                                                ],
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                              ),
                                            ),
                                            child: Center(
                                              child: AutoSizeText(
                                                'Actor',
                                                softWrap: true,
                                                overflow: TextOverflow.ellipsis,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline3
                                                    .copyWith(fontSize: 14),
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  )
                ],
              ),

              ///---top-celebs-widget
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 40, 15, 12),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(
                          'Top 100 Fan Clubs',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontWeight: FontWeight.bold),
                        )),
                        InkWell(
                          onTap: () {
                            Get.to(const TopFanViewScreen());
                          },
                          child: AutoSizeText(
                            'See all',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                        )
                      ],
                    ),
                  ),
                  Wrap(
                    children: List<Widget>.generate(3, (int index) {
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: ListTile(
                          leading: Container(
                            height: 48,
                            width: 48,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                image: const DecorationImage(
                                    image: AssetImage('assets/fifth.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                          title: Text(
                            'Profile Name',
                            style: Theme.of(context).textTheme.headline3,
                          ),
                          subtitle: Text(
                            '2.5k fans',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(
                                    fontSize: 14, color: customLightGreyColor),
                          ),
                          trailing: InkWell(
                            onTap: () {
                              customBottomSheet(context);
                            },
                            child: const SizedBox(
                              width: 100,
                              child: CustomGradientButton(
                                buttonHeight: 40,
                                text: 'Join',
                                textFontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      );
                    }),
                  )
                ],
              ),

              ///---vip-fan-club-widget
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 20, 15, 15),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(
                          'Available for VIP fan Clubs',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontWeight: FontWeight.bold),
                        )),
                        AutoSizeText(
                          'See all',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(color: customLightGreyColor),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 210,
                    width: size.width,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              Get.to(CelebrityProfileScreen());
                            },
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Container(
                                    height: 200,
                                    width: 120,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          height: 140,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            child: Image.asset(
                                              'assets/smith.jpg',
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          'Profile Name',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        ),
                                        const SizedBox(
                                          height: 6,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/video_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/review_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: SizedBox(
                                    height: 200,
                                    width: 120,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        const SizedBox(
                                          height: 80,
                                        ),
                                        if (!homeController.getCategoryCheck)
                                          SkeletonLoader(
                                            highlightColor: Colors.grey,
                                            builder: Column(
                                              children: <Widget>[
                                                Container(
                                                  height: 25,
                                                  width: 80,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                      color: Colors.white),
                                                ),
                                              ],
                                            ),
                                          )
                                        else
                                          Container(
                                            height: 25,
                                            width: 80,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              gradient: const LinearGradient(
                                                colors: <Color>[
                                                  customGradientFirstColor,
                                                  customGradientSecondColor,
                                                ],
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                              ),
                                            ),
                                            child: Center(
                                              child: AutoSizeText(
                                                'Actor',
                                                softWrap: true,
                                                overflow: TextOverflow.ellipsis,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline3
                                                    .copyWith(fontSize: 14),
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  )
                ],
              ),
            ],
          )),
        ),
      ),
    );
  }
}
