import 'package:chewie/chewie.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:video_player/video_player.dart';

class CelebrityVideoScreen extends StatefulWidget {
  const CelebrityVideoScreen({Key key}) : super(key: key);

  @override
  _CelebrityVideoScreenState createState() => _CelebrityVideoScreenState();
}

class _CelebrityVideoScreenState extends State<CelebrityVideoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: VideoItems(
              looping: false,
              autoPlay: true,
              videoPlayerController: VideoPlayerController.asset(
                'assets/video_1.mp4',
              )),
        ),
      ),
    );
  }
}

class VideoItems extends StatefulWidget {
  const VideoItems({
    @required this.videoPlayerController,
    this.looping,
    this.autoPlay,
    Key key,
  }) : super(key: key);
  final VideoPlayerController videoPlayerController;
  final bool looping;
  final bool autoPlay;

  @override
  _VideoItemsState createState() => _VideoItemsState();
}

class _VideoItemsState extends State<VideoItems> {
  ChewieController chewController;

  @override
  void initState() {
    super.initState();
    chewController = ChewieController(
      customControls: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 10, 30, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: SvgPicture.asset(
                      'assets/app_icons/arrows_icon/arrow_left.svg',
                      color: Colors.white,
                    ),
                  ),
                ),
                Text(
                  'Emilia Clarke for Anette Black',
                  style: GoogleFonts.inter(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(7.0),
                            child: SvgPicture.asset(
                              'assets/app_icons/action_icon/download.svg',
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(7.0),
                            child: SvgPicture.asset(
                              'assets/app_icons/action_icon/share.svg',
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                            colors: <Color>[
                              customGradientFirstColor,
                              customGradientSecondColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(7.0),
                            child: SvgPicture.asset(
                              'assets/app_icons/action_icon/report.svg',
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      deviceOrientationsOnEnterFullScreen: <DeviceOrientation>[
        DeviceOrientation.portraitUp
      ],
      deviceOrientationsAfterFullScreen: <DeviceOrientation>[
        DeviceOrientation.portraitUp
      ],
      videoPlayerController: widget.videoPlayerController,
      aspectRatio: 1 / 1.9,
      autoInitialize: true,
      autoPlay: widget.autoPlay,
      looping: widget.looping,
      errorBuilder: (BuildContext context, String errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: const TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    chewController.dispose();
    widget.videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Chewie(
      controller: chewController,
    );
  }
}
