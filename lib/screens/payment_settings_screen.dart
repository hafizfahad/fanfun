import 'package:fanfun/screens/add_payment_card_screen.dart';
import 'package:fanfun/screens/edit_payment_card_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaymentSettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Payments',
          style: Theme.of(context)
              .textTheme
              .headline4
              .copyWith(color: Colors.white),
        ),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Get.to(const AddPaymentCardScreen());
              },
              child: Text(
                'Add',
                style: Theme.of(context).textTheme.headline3.copyWith(
                    fontWeight: FontWeight.w400, color: customOrangeColor),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 10, 20, 19),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                'You link 1 card:',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Text(
              'Personal',
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.white),
            ),
            const SizedBox(
              height: 5,
            ),
            InkWell(
              onTap: () {
                Get.to(const EditPaymentCardScreen(
                  cardNumber: '9429  3810  4829  5829',
                  cardHolder: 'Anna Black',
                  expiryDate: '04/21',
                  cvv: '12345',
                ));
              },
              child: Container(
                height: 200,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(
                    colors: <Color>[
                      customGradientFirstColor.withOpacity(0.4),
                      customGradientSecondColor.withOpacity(0.4),
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            'CARD HOLDER',
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                .copyWith(
                                    color: customWhiteColor, fontSize: 12),
                          ),
                          Text(
                            'Anna Black',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(fontWeight: FontWeight.w700),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        '9429  3810  4829  5829',
                        style: Theme.of(context).textTheme.headline3.copyWith(
                            fontSize: 24, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'CARD EXPIRY',
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                .copyWith(
                                    color: customWhiteColor, fontSize: 12),
                          ),
                          Text(
                            'CVV',
                            style: Theme.of(context)
                                .textTheme
                                .headline4
                                .copyWith(
                                    color: customWhiteColor, fontSize: 12),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '03/21',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(fontWeight: FontWeight.w700),
                          ),
                          Text(
                            '12345',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(fontWeight: FontWeight.w700),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
