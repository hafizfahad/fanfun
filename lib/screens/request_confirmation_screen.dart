import 'package:fanfun/component/gradient_text.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/screens/payment_method_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class RequestConfirmationScreen extends StatefulWidget {
  const RequestConfirmationScreen({Key key}) : super(key: key);

  @override
  _RequestConfirmationScreenState createState() =>
      _RequestConfirmationScreenState();
}

class _RequestConfirmationScreenState extends State<RequestConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Request Confirmation',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: SingleChildScrollView(
            child: Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SizedBox(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 20, 15, 10),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: customPrimaryGreyColor),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Celebrity Name',
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.back();
                                },
                                child: const GradientText(
                                  'Edit',
                                  fontSize: 16,
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      customGradientFirstColor,
                                      customGradientSecondColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          const Divider(
                            color: customLightGreyColor,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                'Pronome:',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(color: customLightGreyColor),
                              )),
                              Expanded(
                                  flex: 2,
                                  child: Text(
                                    pronounValue,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  )),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                'Ocasião:',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(color: customLightGreyColor),
                              )),
                              Expanded(
                                  flex: 2,
                                  child: Text(
                                    occasionValue,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  )),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                'Instruções:',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(color: customLightGreyColor),
                              )),
                              Expanded(
                                  flex: 2,
                                  child: Text(
                                    instructionController.text,
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  InkWell(
                    onTap: () {
                      Get.to(const PaymentMethodScreen());
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: const LinearGradient(
                          colors: <Color>[
                            customGradientFirstColor,
                            customGradientSecondColor,
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'Request \$25',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(fontSize: 18),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        )),
      ),
    );
  }
}
