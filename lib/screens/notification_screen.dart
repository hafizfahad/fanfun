import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List<Map<String, dynamic>> notificationList = <Map<String, dynamic>>[
    <String, dynamic>{
      'image': 'assets/profile_avatar.png',
      'time': '5m ago',
      'name': 'Guy Hawkins',
      'message': 'Liked your message'
    },
    <String, dynamic>{
      'image': 'assets/profile_avatar.png',
      'time': '5m ago',
      'name': 'Guy Hawkins',
      'message': 'Liked your message'
    },
    <String, dynamic>{
      'image': 'assets/profile_avatar.png',
      'time': '5m ago',
      'name': 'Guy Hawkins',
      'message': 'Liked your message'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          'Notifications',
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
        actions: <Widget>[
          InkWell(
            onTap: () {
              setState(() {
                notificationList = <Map<String, dynamic>>[];
              });
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 17, 10, 0),
              child: Text(
                'Clear',
                style: Theme.of(context)
                    .textTheme
                    .headline3
                    .copyWith(color: customOrangeColor),
              ),
            ),
          )
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
        child: notificationList.isEmpty
            ? Center(
                child: Text(
                  'Your Notifications are empty!',
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              )
            : ListView(
                children:
                    List<Widget>.generate(notificationList.length, (int index) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                    child: ListTile(
                      leading: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            image: DecorationImage(
                                image: AssetImage(
                                    '${notificationList[index]['image']}'),
                                fit: BoxFit.fill)),
                      ),
                      title: Row(
                        children: <Widget>[
                          Text(
                            '${notificationList[index]['name']} ',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                            child: Text('${notificationList[index]['message']}',
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.headline3),
                          )
                        ],
                      ),
                      subtitle: Text(
                        '${notificationList[index]['time']}',
                        style: Theme.of(context).textTheme.headline3.copyWith(
                            fontSize: 14, color: customLightGreyColor),
                      ),
                    ),
                  );
                }),
              ),
      ),
    );
  }
}
