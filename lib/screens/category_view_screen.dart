import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/screens/celebrity_profile_screen.dart';
import 'package:fanfun/screens/featured_celebrities_view_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CategoryViewScreen extends StatefulWidget {
  const CategoryViewScreen({this.categoryName, this.categoryColor});
  final String categoryName;
  final Color categoryColor;

  @override
  _CategoryViewScreenState createState() => _CategoryViewScreenState();
}

class _CategoryViewScreenState extends State<CategoryViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: SvgPicture.asset(
              'assets/app_icons/arrows_icon/arrow_left.svg',
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ),
        ),
        elevation: 0,
        title: Text(
          widget.categoryName,
          style: Theme.of(context).textTheme.headline1,
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ///---featured-widget
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 40, 15, 15),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(
                          'Features',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontWeight: FontWeight.bold),
                        )),
                        InkWell(
                          onTap: () {
                            Get.to(const FeaturedCelebrityViewScreen());
                          },
                          child: AutoSizeText(
                            'See all',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(color: customLightGreyColor),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 210,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              Get.to(CelebrityProfileScreen());
                            },
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Container(
                                    height: 200,
                                    width: 120,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          height: 140,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            child: Image.asset(
                                              'assets/seventh.jpg',
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          'Profile Name',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        ),
                                        const SizedBox(
                                          height: 6,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/video_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/review_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: SizedBox(
                                    height: 200,
                                    width: 120,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        const SizedBox(
                                          height: 80,
                                        ),
                                        Container(
                                          height: 25,
                                          width: 80,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: widget.categoryColor),
                                          child: Center(
                                            child: AutoSizeText(
                                              widget.categoryName,
                                              softWrap: true,
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline3
                                                  .copyWith(fontSize: 14),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  )
                ],
              ),

              ///---vip-fan-club-widget
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 30, 15, 15),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(
                          'Available for VIP fan Clubs',
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontWeight: FontWeight.bold),
                        )),
                        AutoSizeText(
                          'See all',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              .copyWith(color: customLightGreyColor),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 210,
                    width: MediaQuery.of(context).size.width,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              Get.to(CelebrityProfileScreen());
                            },
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Container(
                                    height: 200,
                                    width: 120,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius:
                                            BorderRadius.circular(15)),
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          height: 140,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            child: Image.asset(
                                              'assets/smith.jpg',
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 15,
                                        ),
                                        Text(
                                          'Profile Name',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        ),
                                        const SizedBox(
                                          height: 6,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/video_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 5),
                                                  child: SvgPicture.asset(
                                                      'assets/app_icons/other_icon/review_action.svg',
                                                      width: 20,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  '250',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline3
                                                      .copyWith(fontSize: 14),
                                                )
                                              ],
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: index == 0
                                      ? const EdgeInsets.fromLTRB(15, 0, 10, 0)
                                      : const EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: SizedBox(
                                    height: 200,
                                    width: 120,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        const SizedBox(
                                          height: 80,
                                        ),
                                        Container(
                                          height: 25,
                                          width: 80,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: widget.categoryColor),
                                          child: Center(
                                            child: AutoSizeText(
                                              widget.categoryName,
                                              softWrap: true,
                                              overflow: TextOverflow.ellipsis,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline3
                                                  .copyWith(fontSize: 14),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
