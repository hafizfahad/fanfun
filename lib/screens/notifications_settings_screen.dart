import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class NotificationsSettingsScreen extends StatefulWidget {
  const NotificationsSettingsScreen({Key key}) : super(key: key);

  @override
  _NotificationsSettingsScreenState createState() =>
      _NotificationsSettingsScreenState();
}

class _NotificationsSettingsScreenState
    extends State<NotificationsSettingsScreen> {
  bool status1 = false;
  bool status2 = true;
  bool status3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Notifications',
          style: Theme.of(context)
              .textTheme
              .headline4
              .copyWith(color: Colors.white),
        ),
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Column(
            children: <Widget>[
              ///notifications 1
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Notifications type 1',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: Colors.white),
                  ),
                  FlutterSwitch(
                    // /width: 40.0,
                    // height: 55.0,

                    activeColor: Colors.white,
                    inactiveColor: customLightGreyColor,
                    inactiveToggleColor: customPrimaryGreyColor,
                    valueFontSize: 25.0,
                    activeToggleColor: customOrangeColor,
                    toggleSize: 45.0,
                    value: status1,
                    borderRadius: 30.0,
// activeColor: Colors.white,
                    onToggle: (bool val) {
                      setState(() {
                        status1 = val;
                      });
                    },
                  ),
                ],
              ),

              const SizedBox(
                height: 20,
              ),

              ///notifications 2
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Notifications type 2',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: Colors.white),
                  ),
                  FlutterSwitch(
                    // /width: 40.0,
                    // height: 55.0,
                    activeColor: Colors.white,
                    inactiveColor: customLightGreyColor,
                    inactiveToggleColor: customPrimaryGreyColor,
                    valueFontSize: 25.0,
                    activeToggleColor: customOrangeColor,
                    toggleSize: 40.0,
                    value: status2,
                    borderRadius: 30.0,
// activeColor: Colors.white,
                    onToggle: (bool val) {
                      setState(() {
                        status2 = val;
                      });
                    },
                  ),
                ],
              ),

              const SizedBox(
                height: 20,
              ),

              ///notifications 3
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Notifications type 3',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: Colors.white),
                  ),
                  FlutterSwitch(
                    // /width: 40.0,
                    // height: 55.0,
                    activeColor: Colors.white,
                    inactiveColor: customLightGreyColor,
                    inactiveToggleColor: customPrimaryGreyColor,
                    valueFontSize: 25.0,
                    activeToggleColor: customOrangeColor,
                    toggleSize: 40.0,
                    value: status3,
                    borderRadius: 30.0,
// activeColor: Colors.white,
                    onToggle: (bool val) {
                      setState(() {
                        status3 = val;
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
