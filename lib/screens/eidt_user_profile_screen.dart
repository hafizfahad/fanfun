import 'package:auto_size_text/auto_size_text.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/data/global_decoration_data.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class EditUserProfileScreen extends StatefulWidget {
  @override
  _EditUserProfileScreenState createState() => _EditUserProfileScreenState();
}

class _EditUserProfileScreenState extends State<EditUserProfileScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    editFirstNameController.text = 'Annete';
    editLastNameController.text = 'Black';
    editNickNameController.text = 'anna_black';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: AutoSizeText(
          'Edit Profile',
          style: Theme.of(context).textTheme.headline2.copyWith(fontSize: 25),
        ),
        centerTitle: true,
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Get.back();
              },
              child: Text(
                'Cancel',
                style: Theme.of(context).textTheme.headline3.copyWith(
                    fontWeight: FontWeight.w400, color: customOrangeColor),
              ))
        ],
      ),
      body: SafeArea(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(
                20, 0, 20, MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 40,
                ),
                Center(
                  child: CircleAvatar(
                    backgroundColor: Colors.white.withOpacity(0.5),
                    radius: 45,
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                            borderRadius: BorderRadius.circular(60),
                            child: Image.asset('assets/profile.jpg')),
                        Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: <Color>[
                                    Colors.transparent,
                                    Colors.black.withOpacity(0.8)
                                  ])),
                        ),
                        Positioned(
                            left: 30,
                            bottom: 2,
                            child: SvgPicture.asset(
                                'assets/app_icons/action_icon/photo.svg')),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 25,
                ),

                ///first name section
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: AutoSizeText(
                    'First Name',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontSize: 16),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: TextFormField(
                    style: Theme.of(context)
                        .textTheme
                        .headline3
                        .copyWith(fontWeight: FontWeight.w400),
                    controller: editFirstNameController,
                    // obscureText: obscureTextValue,
                    decoration: const InputDecoration(
                      isDense: true,
                      filled: true,
                      fillColor: customLightGreyColor,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 15.0, horizontal: 10.0),
                      // focusedBorder: customTextFieldFocusedBorder,
                      // border: customTextFieldBorder,
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Field Required';
                      }
                      return null;
                    },
                  ),
                ),

                const SizedBox(
                  height: 25,
                ),

                ///last name section
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: AutoSizeText(
                    'Last Name',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontSize: 16),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: TextFormField(
                    style: Theme.of(context)
                        .textTheme
                        .headline3
                        .copyWith(fontWeight: FontWeight.w400),
                    controller: editLastNameController,
                    // obscureText: obscureTextValue,
                    decoration: const InputDecoration(
                      isDense: true,
                      filled: true,
                      fillColor: customLightGreyColor,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 15.0, horizontal: 10.0),
                      // focusedBorder: customTextFieldFocusedBorder,
                      // border: customTextFieldBorder,
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Field Required';
                      }
                      return null;
                    },
                  ),
                ),

                const SizedBox(
                  height: 25,
                ),

                ///nick name section
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: AutoSizeText(
                    'Nick Name',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(fontSize: 16),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: TextFormField(
                    style: Theme.of(context)
                        .textTheme
                        .headline3
                        .copyWith(fontWeight: FontWeight.w400),
                    controller: editNickNameController,
                    // obscureText: obscureTextValue,
                    decoration: InputDecoration(
                      isDense: true,
                      filled: true,
                      fillColor: customLightGreyColor,
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 15.0, horizontal: 10.0),
                      focusedBorder: customTextFieldFocusedBorder,
                      border: customTextFieldBorder,
                    ),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Field Required';
                      }
                      return null;
                    },
                  ),
                ),

                const SizedBox(
                  height: 40,
                ),

                ///save button
                Container(
                  height: 50,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: const LinearGradient(
                      colors: <Color>[
                        customGradientFirstColor,
                        customGradientSecondColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'Save',
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          .copyWith(fontSize: 18),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
