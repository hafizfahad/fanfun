import 'package:email_auth/email_auth.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:fanfun/screens/login_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class EmailConfirmationOtpScreen extends StatefulWidget {
  const EmailConfirmationOtpScreen({
    Key key,
    this.email,
    this.verificationID,
    this.newEmail = '',
    this.isGuestCheckOut,
  }) : super(key: key);
  final String verificationID;
  final String email;
  final String newEmail;
  final bool isGuestCheckOut;

  @override
  _EmailConfirmationOtpScreenState createState() =>
      _EmailConfirmationOtpScreenState();
}

class _EmailConfirmationOtpScreenState extends State<EmailConfirmationOtpScreen>
    with SingleTickerProviderStateMixin {
  ///a void funtion to send the OTP to the user
  Future<void> sendOtp() async {
    EmailAuth.sessionName = 'FanFun';
    final bool result =
        await EmailAuth.sendOtp(receiverMail: signUpEmailController.value.text);
    if (result) {
      setState(() {});
    }
  }

  ///a void function to verify if the Data provided is true
  void verify(String otp) {
    if (EmailAuth.validate(
        receiverMail: signUpEmailController.value.text, userOTP: otp)) {
      Get.offAll(LoginScreen());
    } else {
      Get.snackbar('Error', 'Wrong OTP');
    }
  }

  void stopProgressBar() {
    setState(() {
      check = false;
    });
  }

  bool check;
  // Constants
  final int time = 30;
  AnimationController _controller;

  // Variables
  Size _screenSize;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fifthDigit;
  int _sixthDigit;
  int totalTimeInSeconds;
  bool _hideResendButton;

  String userName = '';
  bool didReadNotifications = false;
  int unReadNotificationsCount = 0;

  // Overridden methods
  @override
  void initState() {
    totalTimeInSeconds = time;
    super.initState();
    sendOtp();
    check = false;
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: time))
          ..addStatusListener((AnimationStatus status) {
            if (status == AnimationStatus.dismissed) {
              setState(() {
                _hideResendButton = !_hideResendButton;
              });
            }
          });
    _controller.reverse(
        from: _controller.value == 0.0 ? 1.0 : _controller.value);
    _startCountdown();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return ModalProgressHUD(
      inAsyncCall: check,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: customGradientFirstColor,
          elevation: 0.0,
          leading: InkWell(
            borderRadius: BorderRadius.circular(30.0),
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            colors: <Color>[
              customGradientFirstColor,
              customGradientSecondColor
            ],
            end: Alignment.bottomCenter,
            begin: Alignment.topCenter,
          )),
          width: _screenSize.width,
//        padding: new EdgeInsets.only(bottom: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Verification Code',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1,
                // new TextStyle(
                //     fontSize: 28.0, color: Colors.white, fontWeight: FontWeight.bold),
              ),
              Text(
                'Please enter'
                ' the OTP sent\non your '
                'registered Email.\n${signUpEmailController.text.toString()}',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 18),
                // new TextStyle(
                //     fontSize: 18.0, color: Colors.white, fontWeight: FontWeight.w600),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpTextField(_firstDigit),
                  _otpTextField(_secondDigit),
                  _otpTextField(_thirdDigit),
                  _otpTextField(_fourthDigit),
                  _otpTextField(_fifthDigit),
                  _otpTextField(_sixthDigit),
                ],
              ),
              if (_hideResendButton)
                SizedBox(
                  height: 32,
                  child: Offstage(
                    offstage: !_hideResendButton,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Icon(
                          Icons.access_time,
                          color: Colors.white,
                        ),
                        const SizedBox(
                          width: 5.0,
                        ),
                        OtpTimer(
                          controller: _controller,
                          fontSize: 15.0,
                        )
                      ],
                    ),
                  ),
                )
              else
                InkWell(
                  onTap: () {
                    _startCountdown();
                    sendOtp();
                  },
                  child: Container(
                    height: 32,
                    width: 120,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(32)),
                    alignment: Alignment.center,
                    child: Text(
                      'Resend OTP',
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          .copyWith(fontSize: 14, color: Colors.black),
                      // new TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                  ),
                ),
              SizedBox(
                  height: _screenSize.width - 80,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            _otpKeyboardInputButton(
                                label: '1',
                                onPressed: () {
                                  _setCurrentDigit(1);
                                }),
                            _otpKeyboardInputButton(
                                label: '2',
                                onPressed: () {
                                  _setCurrentDigit(2);
                                }),
                            _otpKeyboardInputButton(
                                label: '3',
                                onPressed: () {
                                  _setCurrentDigit(3);
                                }),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            _otpKeyboardInputButton(
                                label: '4',
                                onPressed: () {
                                  _setCurrentDigit(4);
                                }),
                            _otpKeyboardInputButton(
                                label: '5',
                                onPressed: () {
                                  _setCurrentDigit(5);
                                }),
                            _otpKeyboardInputButton(
                                label: '6',
                                onPressed: () {
                                  _setCurrentDigit(6);
                                }),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            _otpKeyboardInputButton(
                                label: '7',
                                onPressed: () {
                                  _setCurrentDigit(7);
                                }),
                            _otpKeyboardInputButton(
                                label: '8',
                                onPressed: () {
                                  _setCurrentDigit(8);
                                }),
                            _otpKeyboardInputButton(
                                label: '9',
                                onPressed: () {
                                  _setCurrentDigit(9);
                                }),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            const SizedBox(
                              width: 80.0,
                            ),
                            _otpKeyboardInputButton(
                                label: '0',
                                onPressed: () {
                                  _setCurrentDigit(0);
                                }),
                            _otpKeyboardActionButton(
                                label: const Icon(
                                  Icons.backspace,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  setState(() {
                                    if (_sixthDigit != null) {
                                      _sixthDigit = null;
                                    } else if (_fifthDigit != null) {
                                      _fifthDigit = null;
                                    } else if (_fourthDigit != null) {
                                      _fourthDigit = null;
                                    } else if (_thirdDigit != null) {
                                      _thirdDigit = null;
                                    } else if (_secondDigit != null) {
                                      _secondDigit = null;
                                    } else if (_firstDigit != null) {
                                      _firstDigit = null;
                                    }
                                  });
                                }),
                          ],
                        ),
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    return Container(
      width: 35.0,
      height: 45.0,
      alignment: Alignment.center,
      decoration: const BoxDecoration(
          border: Border(
              bottom: BorderSide(
        width: 2.0,
        color: Colors.white,
      ))),
      child: Text(digit != null ? digit.toString() : '',
          style: Theme.of(context).textTheme.headline1.copyWith(
                fontSize: 30,
              )),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onPressed,
        borderRadius: BorderRadius.circular(40.0),
        child: Container(
          height: 80.0,
          width: 80.0,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Text(label,
                style: Theme.of(context).textTheme.headline1.copyWith(
                      fontSize: 30,
                    )),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  Widget _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return InkWell(
      onTap: onPressed,
      borderRadius: BorderRadius.circular(40.0),
      child: Container(
        height: 80.0,
        width: 80.0,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    setState(() {
      _currentDigit = i;
      if (_firstDigit == null) {
        _firstDigit = _currentDigit;
      } else if (_secondDigit == null) {
        _secondDigit = _currentDigit;
      } else if (_thirdDigit == null) {
        _thirdDigit = _currentDigit;
      } else if (_fourthDigit == null) {
        _fourthDigit = _currentDigit;
      } else if (_fifthDigit == null) {
        _fifthDigit = _currentDigit;
      } else if (_sixthDigit == null) {
        _sixthDigit = _currentDigit;

        final String otp = _firstDigit.toString() +
            _secondDigit.toString() +
            _thirdDigit.toString() +
            _fourthDigit.toString() +
            _fifthDigit.toString() +
            _sixthDigit.toString();

        verify(otp);
      }
    });
  }

  Future<void> _startCountdown() async {
    setState(() {
      _hideResendButton = true;
      totalTimeInSeconds = time;
    });
    _controller.reverse(
        from: _controller.value == 0.0 ? 1.0 : _controller.value);
  }

  void clearOtp() {
    _sixthDigit = null;
    _fifthDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    setState(() {});
  }
}

class OtpTimer extends StatelessWidget {
  const OtpTimer(
      {this.controller, this.fontSize, this.timeColor = Colors.white});
  final AnimationController controller;
  final double fontSize;
  final Color timeColor;

  String get timerString {
    final Duration duration = controller.duration * controller.value;
    if (duration.inHours > 0) {
      return '${duration.inHours}'
          ':${duration.inMinutes % 60}'
          ':${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
    }
    return '${duration.inMinutes % 60}'
        ':${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  Duration get duration {
    final Duration duration = controller.duration;
    return duration;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context, Widget child) {
          return Text(
            timerString,
            style: TextStyle(
                fontSize: fontSize,
                color: timeColor,
                fontWeight: FontWeight.w600),
          );
        });
  }
}
