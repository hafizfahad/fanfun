import 'package:fanfun/screens/email_confirmation_otp_screen.dart';
import 'package:fanfun/screens/login_screen.dart';
import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EmailConfirmationDecisionScreen extends StatefulWidget {
  @override
  _EmailConfirmationDecisionScreenState createState() =>
      _EmailConfirmationDecisionScreenState();
}

class _EmailConfirmationDecisionScreenState
    extends State<EmailConfirmationDecisionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                  child: SizedBox(
                width: MediaQuery.of(context).size.width * .6,
                child: Image.asset('assets/splashIcon.png'),
              )),

              ///------------email-confirmation-button
              Padding(
                padding:
                    const EdgeInsets.only(left: 20.0, right: 20.0, top: 64.0),
                child: InkWell(
                  onTap: () {
                    Get.to(const EmailConfirmationOtpScreen());
                  },
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: const LinearGradient(
                        colors: <Color>[
                          customGradientFirstColor,
                          customGradientSecondColor,
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        'Confirm Email',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(fontSize: 18),
                      ),
                    ),
                  ),
                ),
              ),

              ///------------go-for-login-button
              Padding(
                padding:
                    const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
                child: InkWell(
                  onTap: () {
                    Get.offAll(LoginScreen());
                  },
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: const LinearGradient(
                        colors: <Color>[
                          customGradientFirstColor,
                          customGradientSecondColor,
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        'Go For Login',
                        style: Theme.of(context)
                            .textTheme
                            .headline3
                            .copyWith(fontSize: 18),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              )
            ],
          ),
        ),
      ),
    );
  }
}
