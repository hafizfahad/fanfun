import 'dart:developer';
import 'package:dio/dio.dart' as dio_instance;
import 'package:fanfun/controllers/app_controller.dart';
import 'package:fanfun/screens/bottom_nav_bar_page.dart';
import 'package:fanfun/screens/email_confirmation_decision_screen.dart';
import 'package:fanfun/services/header_services.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> postMethod(String url, Map<String, dynamic> data, String apiCall,
    BuildContext context, Function validator) async {
  final GetStorage box = GetStorage();
  dio_instance.Response<dynamic> response;
  final dio_instance.Dio dio = dio_instance.Dio();

  setHeaderAccept(dio);
  setHeaderContent(dio);

  if (apiCall != 'login' && apiCall != 'register') {
    setHeader(dio, 'Authorization', 'Bearer ${box.read('accessToken')}');
  }

  try {
    log('--------------------$url');
    log('--------------------$data');
    response = await dio.post(url, data: data);
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);

    if (apiCall == 'register') {
      log('registerStatusCode---->> ${response.statusCode}');
      log('registerResponse---->> ${response.data}');
      Get.offAll(EmailConfirmationDecisionScreen());
      Get.snackbar('Login Please', '');
    } else if (apiCall == 'login') {
      log('loginStatusCode---->> ${response.statusCode}');
      log('loginResponse---->> ${response.data}');
      box.write('accessToken', response.data);
      box.write('isLogin', 'true');
      log("Token--->> ${box.read("accessToken")}");
      Get.offAll(BottomNavBAr());
    }
  } on dio_instance.DioError catch (e) {
    Get.find<AppController>().updateLoaderProgressCheck(updatedValue: false);
    Get.snackbar('Error', '${e.response.data['message']}',
        backgroundColor: Colors.white70);
    log('Post Error ------->> ${e.response.data['message']}');
  }
}
