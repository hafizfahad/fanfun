String serverApi = 'https://fanfun.com.br/api/';

String getCategoryApi = '${serverApi}categories';
String loginApi = '${serverApi}sanctum/token';
String registrationApi = '${serverApi}register';
String fcmTokenApi = '${serverApi}user/fcm';
