import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:fanfun/controllers/home_page_controller.dart';
import 'package:fanfun/data/global_data.dart';
import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';

dynamic getMethod(String url, BuildContext context, String apiCall,
    Map<String, dynamic> extraData) async {
  Response<Map<String, dynamic>> response;
  final Dio dio = Dio();

  try {
    response = await dio.get(url, queryParameters: extraData);
    switch (apiCall) {
      case 'categories':
        return <void>{
          if (response.statusCode == 200)
            <void>{
              log('categories --->> ${response.data}'),
              getCategoriesList = response.data['data'] as List<dynamic>,
              Get.find<HomeController>()
                  .changeGetCategoryCheck(updatedValue: true),
            }
          else
            <void>{
              Get.find<HomeController>()
                  .changeGetCategoryCheck(updatedValue: true),
            }
        };
        break;
      default:
    }
  } on DioError catch (e) {
    log('check me get Method Here');
    log('Get Error -------->> ${e.message}');
  }
  // if rest api success data reutrn

  // if success false then show toast
}
