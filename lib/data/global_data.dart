import 'package:fanfun/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

GetStorage box = GetStorage();

List<dynamic> getCategoriesList = <dynamic>[];
TextEditingController signUpEmailController = TextEditingController();

String smartLookSetupKey = '';

//<<<<<<<<<<<<<<<<----------  Home Screen Data  --------------->>>>>>>>>>>>>//
List<Color> categoryColorsList = <Color>[
  customCategoryYellowColor,
  customCategoryGreenColor,
  customCategoryBlueColor,
  customCategoryPurpleColor,
  customCategoryRedColor,
  customCategoryYellowColor,
  customCategoryGreenColor,
  customCategoryBlueColor,
  customCategoryPurpleColor,
  customCategoryRedColor,
  customCategoryYellowColor,
  customCategoryGreenColor,
  customCategoryBlueColor,
  customCategoryPurpleColor,
  customCategoryRedColor,
];
//<<<<<<<<<<<<<<<<----------  Request screen --------------->>>>>>>>>>>>>//
TextEditingController instructionController = TextEditingController();
TextEditingController inController = TextEditingController();
TextEditingController friendNameController = TextEditingController();
String pronounValue = '';
String occasionValue = '';

//<<<<<<<<<<<<<<<<----------  Edit User Profile screen --------------->>>>>>>>>>>>>//
TextEditingController editFirstNameController = TextEditingController();
TextEditingController editLastNameController = TextEditingController();
TextEditingController editNickNameController = TextEditingController();

//<<<<<<<<<<<<<<<<----------  Order Stepper Screen Data  --------------->>>>>>>>>>>>>//

List<String> stepperSubTitleList = <String>[
  'Check your email example@gmail.com',
  'Celebrity has 6 days to respond to your request.',
  'When your order is ready, we will send you an email/sms for you to view, share or download your video.',
];
List<String> stepperTitleList = <String>[
  'Email Notification',
  'Response Time',
  'Ready Order Notification',
];
